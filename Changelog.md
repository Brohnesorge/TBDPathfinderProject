# **--- UPDATE 1.0 [Oct 8, 2023]---**

## - GENERAL -
- New alternative HUD style added
- Added button for removing EXP from the map. See Options
- Puff VFX tweaked
- Potions sprites size increased slightly
- Potion brightmaps added 
- Skull sprite size decreased
- EXP sprite size increased slightly 
- EXP now floats higher off the ground
- Steel Key drop chance decreased
- Fixed Chests causing blood splatter
- Fixed Chests being openable after bashing them open 
- Fixed Chests being bashable after opening them 
- Updated readme

## - CHARACTERS -
- New stat added: **B**ase **A**ttack **S**peed
  - BAS determines the starting speed of attacks, in tics
  - Hidden stat, used in various calculations 
- Windup animation is now consistent across all characters
### Kei
- General 
  - No changes 
- Feint
  - No changes 
- Crane Stance
  - No changes
- Dragon Stance
  - No changes
### Derzan
- General 
  - No changes
- Block
  - Fixed starting with more Stance than intended
- Vital Strike
  - Speed scales off of Rank and Attack 
- Armor Mastery
  - No changes
### Maal'Taal
- General 
  - Attack animation timing tweaked
- Magic Missile
- Spell Combat
  - Start up time decreased 
  - Spell cast time now scales with BAS 
  - Spell recovery time now scales with BAS and Attack
- Magus Arcana
  - Use time is now consistent


