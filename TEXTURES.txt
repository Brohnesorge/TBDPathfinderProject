// Texture definitions generated by SLADE3
// on Wed Oct  4 13:07:25 2023

Sprite "ARMMA0", 41, 38
{
	Offset 21, 37
	Patch "MAGEARMR", 0, 0
}

Sprite "ARMMB0", 41, 38
{
	Offset 21, 37
	Patch "MAGEARMR", 0, 0
	Patch "MAGEARMR", 0, 0
	{
		Translation "80:111=#[0,255,255]", "5:8=#[0,255,255]"
		Alpha 0.10
		Style Add
	}
}

Sprite "ARMMC0", 41, 38
{
	Offset 21, 37
	Patch "MAGEARMR", 0, 0
	Patch "MAGEARMR", 0, 0
	{
		Translation "80:111=#[0,255,255]", "5:8=#[0,255,255]"
		Alpha 0.20
		Style Add
	}
}

Sprite "ARMMD0", 41, 38
{
	Offset 21, 37
	Patch "MAGEARMR", 0, 0
	Patch "MAGEARMR", 0, 0
	{
		Translation "80:111=#[0,255,255]", "5:8=#[0,255,255]"
		Alpha 0.30
		Style Add
	}
}

Sprite "ARMME0", 41, 38
{
	Offset 21, 37
	Patch "MAGEARMR", 0, 0
	Patch "MAGEARMR", 0, 0
	{
		Translation "80:111=#[0,255,255]", "5:8=#[0,255,255]"
		Alpha 0.40
		Style Add
	}
}

Sprite "ARMMF0", 41, 38
{
	Offset 21, 37
	Patch "MAGEARMR", 0, 0
	Patch "MAGEARMR", 0, 0
	{
		Translation "80:111=#[0,255,255]", "5:8=#[0,255,255]"
		Alpha 0.50
		Style Add
	}
}

Sprite "ARMMG0", 41, 38
{
	Offset 21, 37
	Patch "MAGEARMR", 0, 0
	Patch "MAGEARMR", 0, 0
	{
		Translation "80:111=#[0,255,255]", "5:8=#[0,255,255]"
		Alpha 0.60
		Style Add
	}
}

Sprite "ARMMH0", 41, 38
{
	Offset 21, 37
	Patch "MAGEARMR", 0, 0
	Patch "MAGEARMR", 0, 0
	{
		Translation "80:111=#[0,255,255]", "5:8=#[0,255,255]"
		Alpha 0.70
		Style Add
	}
}

Sprite "ARMMI0", 41, 38
{
	Offset 21, 37
	Patch "MAGEARMR", 0, 0
	Patch "MAGEARMR", 0, 0
	{
		Translation "80:111=#[0,255,255]", "5:8=#[0,255,255]"
		Alpha 0.80
		Style Add
	}
}

Sprite "ARMMJ0", 41, 38
{
	Offset 21, 37
	Patch "MAGEARMR", 0, 0
	Patch "MAGEARMR", 0, 0
	{
		Translation "80:111=#[0,255,255]", "5:8=#[0,255,255]"
		Alpha 0.90
		Style Add
	}
}

// End of texture definitions
