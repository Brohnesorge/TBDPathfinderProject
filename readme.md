# **--- BEFORE YOU START  ---**
- Check out the options to map the needed controls
- This mod is very melee focused, so choose what maps to play carefully
- There is a very high chance the Icon of Sin will be unbeatable
- This mod has special interactions with Gun Bonsai, disabling GB xp gain on Sheets and Bags, and blacklisting certain useless upgrades. Load Gun Bonsai first or the blacklisting won't work.
- This mod is not intended to be an accurate representation, or adaptation, of the Pathfinder rules

# - Basic Controls - 

### Weapons 
- Main Fire: Use weapon
- Alt Fire: Use maneuver 
- Reload: Use Ability 1
- Zoom: Use Ability 2
- Weapon State 1: Switch to Potion Rack 
- Weapon State 2: Switch to Scroll Case
- Weapon State 3: View Character Sheet

### Potion Rack
- Main Fire: Drink selected potion
- Alt Fire: Drink selected Mythic potion 
- Reload: Switch to next potion 
- Zoom: Switch to previous potion 
- Weapon State 1: Drop selected potion 
- Weapon State 2: Drop selected Mythic potion

### Scroll Case
- Main Fire: Use selected scroll
- Reload: Switch to next scroll
- Zoom: Switch to previous scroll
- Weapon State 1: Drop selected scroll

### Character Sheet 
- Main Fire: Accept choice
- Alt Fire: Decline choice 
- Reload: Switch to next page 
- Zoom: Switch to previous page 

Character Sheet Pages
1. Character Sheet. Shows all information about your character, including hidden stats like stat growths and ability descriptions
2. Mythic Sheet. Contains info about becoming Mythic. This is also the sheet where you choose to become Mythic or not. See below for more information

# - Gameplay Concepts - 
- Characters have two Abilities and a Maneuver unique to them. Maneuvers scale directly to your character's Level. Abilities scale off of your Ability Rank.
- Each Character starts with a Main Weapon and a Secondary Weapon. 
  - You need to drop your current Main Weapon to pick up a new one. 
  - The Secondary Weapon cannot be discarded, and cannot be Enchanted.
- Melee weapons deal a set amount of damage, while Ranged weapons deal their damage multiplied by a random number up to 8. This is indicated by the Damage field on the HUD.
- Ranged weapons do not require ammo, and are all hitscans.
- All weapons can Critical Hit. The HUD indicates the Crit Chance and Crit Multiplier of the selected weapon. 
- Enemies drop EXP as gems. Collect EXP to Level Up. On Level Up, your stats and HP will go up. Maximum Level is 20.

# - Stats -
There are 5 Stats shown on the HUD. Stats cap at 100. HPD can be either 6, 8, 10 or 12.
 
- HPD: Hit Point Dice. The maximum number that can be rolled when increasing your HP on Level Up. Does not change and is based on Character selected.
- STR: Strength. Increases damage done by melee attacks.
- SKL: Skill. Increases damage done by ranged attacks. At high values, increases your Dodge.
- END: Endurance. Increases the amount of HP you get on Level Up. At high values, increases your Guard.
- MAG: Magic. Increases the effectiveness of scrolls and spells.

There are 4 additional statistics

- Attack determines the speed of your attacks. Each point takes one tick off of the windup time of attacks. Goes up every 4 Levels
- Dodge is the percent chance to dodge a hit, reducing damage to 0. Caps at 85%.
- Guard reduces taken damage by the value percent. Does not apply to environmental damage. Caps at 85%.
- Ability Rank goes up every 5 Levels. Each Rank increases the effectiveness of both Abilities in some way. See Character List for more details.

# - Enchantments -
Instead of finding new weapons, you will find enchanted versions of your Main Weapon. Generally, the better the base weapon spawn, the better the Enchantments will be.
En. Grade, on the HUD, is the Enchantment Grade of the weapon. All Enchanted weapons deal additional Magic damage based on this amount. The higher the En. Grade, the more bonus damage done.
Enchantments themselves get more effective as Enchantment Grade rises. Enchantment Grade can not be raised, and Enchantments can not be changed on a weapon. You will need to find a new one.

Enchantments fall into three Categories. You can only have up to 1 Enchantment in each Category. There will be a list of Enchantments and their effects at the end of this readme. 

- Elemental: Adds bonus Elemental damage to attacks
- Utility: Adds various bonuses to the weapon
- Critical: Adds various bonuses to the critical hits of the weapon

# - Potions - 
Instead of powerups, you will find various potions. You can see your inventory of potions on the HUD. The first number is normal potions, and the second number is Mythic potions.
Potions spawned have a low chance to be Mythic. Mythic potions are much stronger versions of the potion.
You can see what potion you have selected on the HUD. This info replaces the weapon info when the Potion Rack is selected.

There are seven types of potions you can find (Mythic effects look like this):

- Cure Wounds: Heals 25 HP (50% HP)
- Endure Elements: Protects you from environmental hazards for 300 seconds. (Additonally grants 50% resistance to Fire/Ice/Electric/Magic)
- Invisibility: Makes you harder to target, throwing off enemies' aim and granting +20% Dodge for 120 seconds. (Additional +15% Dodge and lasts 300 seconds)
- Haste : Increases Movement speed by 25%, increases Attack by 2 and grants +5% Dodge for 120 seconds (Additionally doubles attack speed and grants additional +15% Dodge)
- Regeneration: Heals 1 HP per second for 300 seconds (Heals 3 HP per second for 600 seconds and persists through maps)
- Protection: Grants +33% Guard for 120 seconds (Additional +17% Guard)
- Heroism: Doubles damage dealt, doubles attack speed, grants +15% Guard and Dodge for 60 seconds (lasts 120 seconds)

# - Scrolls -
Scrolls replace large ammo pickups. You can see your inventory of scrolls on the HUD. They have a low chance to spawn but possess powerful effects. You can see what scroll you have selected on the HUD. This info replaces the weapon info when the Scroll Case is selected.

There are six types of scrolls you can find, three offensive, and three defensive:

- Fireball: Fires a projectile that explodes, dealing Fire damage on hit and in an area
- Lightning Bolt: A hitscan attack that deals set Electric damage
- Ice Storm: Conjures an blizzard in a large area around you, dealing constant Ice damage
- Cause Fear: Inflicts Fear in a large area around you, lasting for 4 seconds, and enemies that are close deal less damage and take more damage for a short time
- Magic Weapon: Increases Weapon Damage by a percent amount for a minute
- Fortify: Grants bonus HP

# - Treasure Chests -
Backpacks will be replaced by locked treasure chests. These chests contain a weapon of Enchantment Grade 3 or higher, a large amount of potions, a number of scrolls and a random amount of EXP between 500 and 5000.

There are two ways to open the chest:

1. Bash it open. Chests only take damage from melee attacks and have 100 HP. There is a 90% chance to destroy the contents.
2. Use a Steel Key. Steel Keys can drop from any enemy at a rate of EnemyHP/7500. You can only carry one Steel Key at a time.

# - Feats -
**- UNDER CONSTRUCTION -**

Feats will be additional progression for your character. You get your first Feat at Level 2, then an additional Feat every 3 Levels afterwards.
Most are passive effects, but some will unlock additional commands. You can choose to spend the Feat on a Feat of that level or a lower level.
You can also save Feats for later levels.

# - Mythic Character  -
At Level 10, you will gain the option to become a Mythic Character. 
This is only offered at Level 10, is irreversible, and if you decline it or Level Up again, *the choice is lost forever.*
Mythic Characters will gain better Stat Growths, increased HPD (including being able to go up to 20), a higher Stat cap of 150 and access to Mythic Feats.
However, the amount of EXP required to Level Up increases drastically.

Page 2 of the Character Sheet is where you make your choice for Mythic.

# --- Options and Log ---
## - Options -
### Gameplay Options
* Auto Select Potion of Cure Wounds

When enabled, the Potion Rack will default to Cure Wounds when selected, instead of remembering the last potion selected
* EXP Multiplier

Multiplies EXP drops by the amount selected

* Bonus EXP from flags

When calculating the amount of EXP an enemy will drop, any flags they have increase the amount of EXP they will drop. Flags include BOSS, MISSILEMORE, FLOAT, NOPAIN, etc. This option toggles that.

### Visual Options

* HUD Style 

Switch between the character sheet like Text HUD, or a Graphic only HUD

* Big Text Color 

Controls the color of the large header text on the Text HUD

* Small Text Color 

Controls the color of the small info text on the Text HUD

* Level Up Message

Toggles the LEVEL UP text that appears on a Level Up

* Level Up Sound

Toggles the sound effect that plays on a Level Up

* Level Up Details

Toggles the stat increases from a Level Up appearing in the log

* VFX Toggles

Toggles various visual effects. The labels are pretty self-explanatory.

* VFX Style 

Toggles between various visual effects using 'Fancy' new sprites, or using 'Classic', recolored Doom sprites

### Log Options

This mod features options for displaying various game events and other information in the message log, emulating a sort of digital Game Master. A list is below, and these are explained in greater detail in the Options Menu itself. Some logs may spoil information you didn't want to know, and having many logging options enabled at once will flood the message log. Use discretion when enabling.

* Player Damage Taken
* Player Healing
* Total Damage Dealt
* Weapon Damage Dealt
* Magic Damage Dealt
* Elemental Damage Dealt
* Dodges
* Critical Hits
* EXP Drops
* EXP Gained On Pickup
* Enemy Spawns
* Monster Death
* Monster Resurrection
* Kei's Feints
* Maal'Taal's Spell Casts
* Derzan's Block

# --- Character List ---

- HP: The starting HP of the character (The Hit Point Dice of the character)
- STR/SKL/END/MAG: The starting Stat of the character. (The Growth of that Stat, being either Poor, Fine or Good)
- Focus Stat: The preferred Stat of that character. Used for certain Feats and Enchantments
- Maneuver: The maneuver of the character 
- Ability 1: The Reload Ability of the character
- Ability 2: The Zoom Ability of the character
- Main Weapon: The Main Weapon of the character, the weapon's damage, the weapon's Crit Chance and Crit Multiplier
- Secondary Weapon: The Secondary Weapon of the character, the weapon's damage, the weapon's Crit Chance and Crit Multiplier, the weapon's type
- Special: Any special rules or factors that are unique to the character

### Kei, Tengu, Swordmaster
- HP: 75 (d8)
- STR: 10 (Fine)
- SKL: 20 (Good)
- END: 10 (Poor)
- MAG: 10 (Fine)
- Focus Stat: Skill 
- Maneuver : Feint
- Ability 1: Dragon Trance
- Ability 2: Crane Trance
- Main Weapon: Katana, 20 DMG, 10% x2 Crit, Melee
- Secondary Weapon: Short bow, 3 DMG, 5% x3 Crit, Ranged 

Son of a ninja matriarch, has (mostly) abandoned his ninja ways to master the sword. Cunning, cruel and completely devoid of honor.

Feint: Stuns the target with a false swipe. Fails on high HP targets. Stunned targets cannot move and take 2.5x damage for a short time.
- Fails on targets with 100 plus 50 per level HP or more. Always fails on bosses and enemies that feel no pain.

Trances last 60 seconds. Afterwards, Kei is fatigued for 20 seconds, lowering his speed and Attack. Only one trance can be active at a time.

1. Dragon Trance: Increases STR bonus to melee attacks by a large amount.
   - Rank 1: Multiplies STR bonus by 2.5. 
   - Rank 2: Multiplies STR bonus by 3.0. Duration increases to 75 seconds.
   - Rank 3: Multiplies STR bonus by 3.5. Duration increases to 90 seconds.
   - Rank 4: Multiplies STR bonus by 4.0. Both Trances may be active at once. Duration increases to 105 seconds.
   - Rank 5: Multiplies STR bonus by 4.5. Duration increases to 120 seconds.
2. Crane Trance: Lowers Attack, but grants Dodge and improves Feint.
   - Rank 1: +35% Dodge. Lowers Attack by 4. Feint now fail on targets with 100 plus 100 per Character Level HP or more. Feinted targets now take 3.5x damage.
   - Rank 2: +40% Dodge. Attacks can now deflect projectiles. Duration increases to 75 seconds.
   - Rank 3: +45% Dodge. No longer lowers Attack. Duration increases to 90 seconds.
   - Rank 4: +50% Dodge. Both Trances may be active at once. Duration increases to 105 seconds.
   - Rank 5: +55% Dodge. Duration increases to 120 seconds. 

### Derzan, Human, Armor Master 
- HP: 125 (d10)
- STR: 15 (Fine)
- SKL: 5 (Poor)
- END: 20 (Good)
- MAG: 0 (Poor)
- Focus Stat: Endurance
- Maneuver: Block
- Ability 1: Armor Training
- Ability 2: Vital Strike
- Main Weapon: Mace and Shield, 15 DMG, 5% x2 Crit, Melee 
- Secondary Weapon: Heavy Crossbow, 10 DMG, 5% x3 Crit, Ranged

Deserter turned mercenary. Stoic wall of a man, shockingly brave if unlucky. 

Block: Blocks attacks from the front. Can only block so much damage before guard is broken, stunning Derzan. While stunned, Derzan cannot attack and moves slower.
- Can block up to 50 plus 10 per Level damage before guard breaks. Recovers 1 guard per tic. Guard break stuns Derzan for 2 seconds 

1. Armor Training: Passive. Increases the effectiveness of worn armor.
   - Rank 1: Max Armor Amount raised to 400. Armor Save Percent is +10%.
   - Rank 2: Slowly regenerates Armor Amount when it is above 0 at a rate of 1 Armor per second, up to a max of 100.
   - Rank 3: Gain +25% base Guard.
   - Rank 4: Become immune to radius damage and gain 1 HP per second Health Regeneration as long as armor is worn.
   - Rank 5: Armor Regeneration increases to 3 Armor per second.  As long as you have armor, Derzan cannot die.
2. Vital Strike: A slow, steady attack that deals multiplied Base weapon damage.
   - Rank 1: Multiplies Base Damage by 3. Increases Crit Multiplier by 1.0.
   - Rank 2: Multiplies Base Damage by 4. Increases Crit Multiplier by 1.5.
   - Rank 3: Multiplies Base Damage by 5. Increases Crit Multiplier by 2.0. +10% Crit Chance.
   - Rank 4: Multiplies Base Damage by 6. Increases Crit Multiplier by 3.5. 
   - Rank 5: Multiplies Base Damage by 7. Increases Crit Mulitplier by 4.0.

### Maal'Taal, Lizardfolk, Magus
- HP: 100 (d8) 
- STR: 20 (Fine)
- SKL: 10 (Fine)
- END: 15 (Fine)
- MAG: 15 (Fine)
- Focus Stat: STR
- Maneuver: Magic Missile
- Ability 1: Magus Arcana
- Ability 2: Spell Combat
- Main Weapon: Black Blade, 30 DMG, 5% x2 Crit, Melee
- Secondary Weapon: None 
- Special: Does not find Enchanted weapons. Instead, the Black Blade's En. Grade increases as Maal'Taal's Level increases. Maximum En. Grade is 10 instead of 5.

Lizardfolk shaman, of the Taal clan from the Great Swamp. Bound to a demonic Black Blade. Proficient in its use and skilled in the arcane arts. Hopelessly affectionate for a dragon blooded human woman.

Magic Missile: Shoots a weak magic projectile.
- Deals 5 Magic damage and has homing. Fires an additional missile every 3 Levels. Has weaker Magic scaling than most spells.

1. Magus Arcana: Randomly generates Enchantments on the Black Blade. There is always a 1% chance to get Profane. Holy can never be rolled. Has a cooldown of 120 seconds.
   - Rank 1: Vorpal, Dreadful, Deadly, Masterful, Crushing, Bolstered, Agile, Arcane and Profane cannot be rolled.
   - Rank 2: Cooldown reduced to 100 seconds. Deadly, Crushing, Agile, Arcane and Bolstered are added to the pool.
   - Rank 3: Cooldown reduced to 80 seconds. Dreadful and Masterful are added to the pool. Guarantees all Enchantment Slots will be filled.
   - Rank 4: Cooldown reduced to 60 seconds. Profane is added to the pool.
   - Rank 5: Cooldown is removed. Vorpal is added to the pool.
2. Spell Combat: Quickly casts a touch spell and makes a follow up empowered strike with the Black Blade. Spells deal additional damage based on Ability Rank.
   - Spell cast is based on the Elemental Enchantment of The Black Blade
     - Fire: Casts Burning Hands. Deals 8d8 per Rank Fire damage.
     - Ice: Casts Chill Touch. Deals 1 - 80 per Rank Ice damage.
     - Electric: Casts Shocking Grasp. Deals 40 per Rank Electric damage. Has extremely low recovery time.
     - Profane: Casts Vampiric Touch. Deals 50 per Rank Unholy damage. Heals you for 25% damage inflicted.
   - Rank 1: Multiplies weapon damage by 1.50. Multiplies Enchantment damage by 1.25. Lowers Attack by 5.
   - Rank 2: Multiplies weapon damage by 1.75. Multiplies Enchantment damage by 1.50. Improved start up and recovery time of spell cast.
   - Rank 3: Multiplies weapon damage by 2.00. Multiplies Enchantment damage by 1.75. Lowers Attack by 2. Random spell is also cast on sword strike.
   - Rank 4: Multiplies weapon damage by 2.25. Multiplies Enchantment damage by 2.00. Elemental damage multiplied by 2.
   - Rank 5: Multiplies weapon damage by 2.50. Multiplies Enchantment damage by 2.25. Raises Attack by 2.

### Amber, Human?, Spell Sage
- HP: 50 (d6) 
- STR: 5 (Poor)
- SKL: 10 (Fine)
- END: 5 (Poor)
- MAG: 20 (Good)
- Focus Stat: MAG
- Maneuver: Spell Cast
- Ability 1: Spell Mastery
- Ability 2: Spell Knowledge
- Main Weapon: Cane, 5 DMG, 5% x2 Crit, Melee
- Secondary Weapon: None 
- Special: Does not find Enchanted weapons. Has a Spellbook bound to Weapon State 4 used to Ready and Prepare spells. Reload and Zoom cycle through spells. Main Fire readies the selected Arcane spell, and Alt Fire readies the selected Divine spell. Readying any spell refreshes Prepared spells. Prepared spells cap is 4 + 2 per Rank

One-eyed scarred woman hailing from a land of steam and rust. Speaks flatly and doesn't blink. Extremely learned about magic, able to cast spells from completely different disciplines.

Spell Cast: Casts the readied Arcane spell, consuming a Prepared Spell in the process. Arcane spells are:
- Magic Missile: Launches 5 per Rank weak homing Magic projectiles, dealing 5 damage each.
- Fireball: Shoots a fireball, dealing 6d8 per Rank Fire damage, and explode for 64 + 64 per Rank Fire damage in a 64 + 64 per Rank radius.
- Lightning Bolt: Shoots a hitscan attack, dealing 50 per Rank Electric damage and overpen on kill. Overpen amount increases with Rank.
- Ice Storm: Summons an ice storm for 4 seconds per Rank, dealing constant Ice damage in a large area. Max damage range and radius increases with Rank.

1. Spell Mastery: Casts the readied Divine spell, consuming a Prepared Spell in the process. Divine Spells are:
	- Searing Light: Fires a fast projectile that 7d7 per Rank Holy damage. Always gibbs, always deals at least 21 per Rank damage and ignores armor.
	- Vampiric Touch: Touch deals 50 per Rank Unholy damage, healing you for 10% per Rank of damage dealt
	- Cure Wounds: Heal 10 HP per Rank
	- Protection from Evil: Reduces damage taken by 50% and grants immunity to Unholy damage. Melee attack damage is reduced by 75% instead, and causes a radius pushback. Lasts 30 seconds per Rank.
2. Spell Knowledge: Taps into forbidden knowledge, entering a heightened state. Lasts for 60 seconds, then goes on cooldown for 120 seconds. While in this state, spells become an higher level version. Goes on cooldown instantly on casting a spell.
	- Rank 1: 60 second duration. 120 second cooldown. Spells become:
		- Fireball -> Meteor Storm: Calls down 3 x (1.5 x Rank rounded down) meteors. Meteors aim at enemies in the area, and deal 15d8 Fire damage and explode for 128 Fire damage in a 128 radius.
		- Lightning Bolt -> Stormbolts: Calls down multple lightning bolts per Rank. Lightning Bolts automatically hit targets in range all around you, dealing heavy Electric damage.
		- Ice Storm -> Polar Ray: Fires a beam dealing extreme Ice damage.
		- Magic Missile -> Disintegrate: Enemies below 100 per Rank HP are utterly destroyed. Enemies with higher HP take heavy Magic damage instead.
		- Searing Light -> Flame Strike: Deals extreme Holy damage in a large area aimed at. 
		- Vampiric Touch -> Massacre: Fires a short ranged grounded shockwave that rips through enemies, instantly killing them. Fails and stopped by very strong enemies and enemies immune to Unholy.
		- Cure Wounds -> Heal: Heals 100 HP per Rank.
		- Protection From Evil -> Sanctuary: Become invincible for 20 seconds per Rank.
	- Rank 2: Cooldown becomes 100 seconds.
   	- Rank 3: Duration becomes 90 seconds. Cooldown becomes 80 seconds.
   	- Rank 4: Cooldown becomes 60 seconds.
   	- Rank 5: Duration becomes 120 seconds. Cooldown becomes 30 seconds.

# --- Enchantment List  ---

### Elemental 
- Fire: Adds 1 - 8 Fire damage per En. Grade 
- Ice: Adds 1 - (16 per En. Grade) Ice damage 
- Bolt: Adds 5 Electric damage per En. Grade 
- Holy: Adds 5 - 20 Holy damage per En. Grade. Rare 
- Profane: Adds 5 - 45 Unholy damage per En. Grade. Rare

### Utility
- Dodging: Adds 25% Dodge 
- Guarding: Adds 25% Guard 
- Crushing: Adds bonus damage based on Strength
- Agile: Adds bonus damage based on Skill 
- Bolstered: Adds bonus damage based on Endurance
- Arcane: Adds bonus damage based on Magic
- Masterful: Adds bonus damage based on character's focus stat 
 
### Critical
- Keen: Doubles Crit Chance 
- Deadly: Increases Crit Multiplier by 1
- Dreadful: Increase Crit Multiplier by 0.5. On a Critical Hit, pushes enemies back and causes Fear in a large area 
- Vorpal: Critcal Hits instantly kill. Rare. Only available on Enchantment Grade 5 weapons

# --- Feat List --- 
# **- UNDER CONSTRUCTION -**

### Level 2
- Toughness : Increases HP per level by 3. Does not add HP retroactively.
- Dodge: Increases base Dodge by 10%
- Endurance: Increases base Guard by 10%

### Level 5
- Power Attack: Allows you to hold attacks to deal more damage. Charges up faster as you level.
- Improved Maneuver: Treats your level as 1.5x higher when caluclating for Maneuvers
- Fleet: Increases base movement speed by 10%. 

### Level 8
- Weapon Finesse: Add your SKL modifier to melee attacks and your STR modifier to ranged attacks
- TBD
- Ability Focus: Abilities count as 1 Rank higher for scaling. Does not unlock additional ability properties. 

### Level 11
- Critical Focus: +5% base Crit Chance
- Mobility: +20% Dodge when moving above a certain speed. Requires Fleet.
- Weapon Focus: Main Weapon Base Damage +5, Crit Chance +5% and Crit Multiplier +0.5. 
- Mythic Arcane Strike: Doubles effective En. Grade.

### Level 14
- Furious Focus: All attacks are Power Attacks. Requires Power Attack.
- Improved Critical: +1 base Crit Multiplier  
- Great Cleave: Melee attacks hit all enemies around you in a burst. Ranged attack gain a AoE. Only affects Main Weapon. 
- Mythic Critical: Automatically Critical Hit enemies with less than 400 HP.

### Level 17
- Death or Glory: On enemy kill, gain a Death or Glory stack for every 100 HP they had. Each stack grants +2% Damage and +1% Guard. Lose all stacks after not attacking for 10 seconds. 
- Deathless Master: Once every level when you would die, fully heal and become invicible for 6 seconds. 
- Strikeback: On Dodge, enemy that attack takes damage equal to a Critcal Hit, ignoring range and defense.
- Mythic Initative: Freezes time for 10 seconds at the start of every stage 

### Level 20
- Critcal Mastery: +10% base Crit Chance, Crit Multiplier is set to x10. Requires Weapon Focus, Improved Critcal and Critical Focus.
- Ultimate Resolve: Three times after you take this feat, when you would die, fully heal and gain a Ultimate Resolve stack. Ultimate Resolve grants health regen of 1HP/sec/stack, increase your Guard and Dodge by 10% per stack, increase your Stats by 10 per stack. 
- Weapon Mastery: Base Damage becomes 50, Attack increase to 10. Requires Weapon Focus, Furious Focus and Great Cleave. 
- Mythic Heritage: +200 Max HP, +50 to all Stats bypassing the cap, Dodge and Guard cap become 99%. 